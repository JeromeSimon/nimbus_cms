import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.util.concurrent.TimeUnit;

public class Login {

    WebDriver driver;
    String URL = "http://dev-cms.nimbuscafe.ph";
    WebDriverWait wait;
    String cont;

    @BeforeClass
    public  void BeforeClass() throws InterruptedException {
        System.setProperty("webdriver.chrome.driver", "C:\\webdrivers\\chromedriver.exe");
    }

    @BeforeMethod
    public void openWindow() throws InterruptedException {
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver,10);
        driver.get(URL);
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
    }

    @Test
    public void TC1_CheckLoginFunctionValidInputs() throws InterruptedException{
<<<<<<< HEAD
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("mineskiAdmin");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("qX<.h8iW");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
        if(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/header/div[1]"))).isDisplayed())
            System.out.println("TC1: Passed");
        else
            Assert.fail("TC1: Failed");
    }

    @Test
    public void TC2_CheckLoginInvalidInputsWrongPassword() throws InterruptedException{
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("mineskiAdmin");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("password");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
        if(cont.contains("Incorrect Credentials"))
            System.out.println("TC2: Passed");
        else
           0;
    }

    @Test
    public void TC3_CheckLoginInvalidInputsWrongUsername() throws InterruptedException{
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("admin");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("qX<.h8iW");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
        if(cont.contains("Incorrect Credentials"))
            System.out.println("TC3: Passed");
        else
            Assert.fail("TC3: Failed");
    }

    @Test
    public void TC4_CheckLoginInvalidInputs() throws InterruptedException{
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("admin");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("password");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
        if(cont.contains("Incorrect Credentials"))
            System.out.println("TC4: Passed");
        else
            Assert.fail("TC4: Failed");
    }

    @Test
    public void TC5_CheckLoginInvalidInputsExceeding() throws InterruptedException{
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("mineskiAdmin");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("qX<.h8iW111");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
        if(cont.contains("The password field cannot exceed 8 characters in length."))
            System.out.println("TC5: Passed");
        else
            Assert.fail("TC5: Failed");
    }

    @Test
    public void TC6_CheckLoginInvalidInputsNoUsername() throws InterruptedException{
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("qX<.h8iW");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
        if(cont.contains("Incorrect Credentials"))
            System.out.println("TC6: Passed");
        else
            Assert.fail("TC6: Failed");
    }

    @Test
    public void TC7_CheckLoginInvalidInputsNoPassword() throws InterruptedException{
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("mineskiAdmin");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
        if(cont.contains("Incorrect Credentials"))
            System.out.println("TC7: Passed");
        else
            Assert.fail("TC7: Failed");
    }

    @Test
    public void TC8_CheckLoginInvalidInputsBlank() throws InterruptedException{
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
        if(cont.contains("Incorrect Credentials"))
            System.out.println("TC8: Passed");
        else
            Assert.fail("TC8: Failed");
    }
=======
>>>>>>> 2afc7c5a24f81dc82d40dc5ce78676fc34ac5bae

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-uid-3"))).sendKeys("anjend");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gwt-uid-5"))).sendKeys("qwe12345");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//div[@role='button' and span='Sign In']"))).click();
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("mineskiAdmin")
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("qX<.h8iW");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
//        if(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/header/div[1]"))).isDisplayed())
//            System.out.println("TC1: Passed");
//        else
//            Assert.fail("TC1: Failed");
    }

//    @Test
//    public void TC2_CheckLoginInvalidInputsWrongPassword() throws InterruptedException{
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("mineskiAdmin");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("password");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
//        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
//        if(cont.contains("Incorrect Credentials"))
//            System.out.println("TC2: Passed");
//        else
//            Assert.fail("TC2: Failed");
//    }
//
//    @Test
//    public void TC3_CheckLoginInvalidInputsWrongUsername() throws InterruptedException{
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("admin");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("qX<.h8iW");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
//        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
//        if(cont.contains("Incorrect Credentials"))
//            System.out.println("TC3: Passed");
//        else
//            Assert.fail("TC3: Failed");
//    }
//
//    @Test
//    public void TC4_CheckLoginInvalidInputs() throws InterruptedException{
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("admin");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("password");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
//        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
//        if(cont.contains("Incorrect Credentials"))
//            System.out.println("TC4: Passed");
//        else
//            Assert.fail("TC4: Failed");
//    }
//
//    @Test
//    public void TC5_CheckLoginInvalidInputsExceeding() throws InterruptedException{
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("mineskiAdmin");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("qX<.h8iW111");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
//        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
//        if(cont.contains("The password field cannot exceed 8 characters in length."))
//            System.out.println("TC5: Passed");
//        else
//            Assert.fail("TC5: Failed");
//    }
//
//    @Test
//    public void TC6_CheckLoginInvalidInputsNoUsername() throws InterruptedException{
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("qX<.h8iW");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
//        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
//        if(cont.contains("Incorrect Credentials"))
//            System.out.println("TC6: Passed");
//        else
//            Assert.fail("TC6: Failed");
//    }
//
//    @Test
//    public void TC7_CheckLoginInvalidInputsNoPassword() throws InterruptedException{
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("mineskiAdmin");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
//        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
//        if(cont.contains("Incorrect Credentials"))
//            System.out.println("TC7: Passed");
//        else
//            Assert.fail("TC7: Failed");
//    }
//
//    @Test
//    public void TC8_CheckLoginInvalidInputsBlank() throws InterruptedException{
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div/form/div[1]")));
//        cont = driver.findElement(By.xpath("/html/body/div[2]/div/form/div[1]")).getText();
//        if(cont.contains("Incorrect Credentials"))
//            System.out.println("TC8: Passed");
//        else
//            Assert.fail("TC8: Failed");
//    }
//
//    @Test
//    public void TC9_CheckLogoutFunctionality() throws InterruptedException{
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"username\"]"))).sendKeys("mineskiAdmin");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"password\"]"))).sendKeys("qX<.h8iW");
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@name=\"submit\"]"))).click();
//        wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"ml-menu\"]/div[2]/div[2]/a"))).isDisplayed();
//        driver.findElement(By.xpath("//*[@id=\"ml-menu\"]/div[2]/div[2]/a")).click();
//        if(wait.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("/html/body/div[2]/div"))).isDisplayed())
//            System.out.println("TC9: Passed");
//        else
//            Assert.fail("TC9: Failed");
//    }

//    @AfterMethod
//    public void closeWindow(){
//        driver.quit();
//    }
}
